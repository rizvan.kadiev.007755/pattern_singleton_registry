"""
Файл registry.py содержит класс Registry, который использует шаблон Одиночка (Singleton)
и представляет реестр для управления устройствами IoT.

Класс Registry обеспечивает следующий функционал:

1. Регистрация устройства: Метод `register_device` позволяет
зарегистрировать устройство IoTв реестре по его серийному номеру.

2. Получение устройства по серийному номеру: Метод `get_device` позволяет
получить зарегистрированное устройство по его серийному номеру.

3. Поиск устройства по серийному номеру: Метод `find_device_by_serial_number` предоставляет
удобный интерфейс для поиска устройства по его серийному номеру (просто вызывает `get_device`).

Класс Registry реализован с использованием шаблона Одиночка (Singleton), что гарантирует,
что в приложении будет только один экземпляр этого класса. Это полезно для централизованного управления устройствами и избегания дублирования данных.

Пример использования класса Registry можно найти в файле main.py,
где создаются экземпляры Registry и выполняются операции регистрации и поиска устройств.

"""

from singleton import MetaSingleton


class Registry(metaclass=MetaSingleton):
    _registry = {}  # Приватное статическое поле, хранящее зарегистрированные устройства

    def register_device(self, serial_number, device):
        if serial_number in self._registry:
            self._registry[serial_number].append(device)
        else:
            self._registry[serial_number] = [device]

    def get_device(self, serial_number):  # Метод для получения устройства по его серийному номеру
        return self._registry.get(serial_number)

    def find_device_by_serial_number(self, serial_number):
        devices = self._registry.get(serial_number, [])
        return devices
