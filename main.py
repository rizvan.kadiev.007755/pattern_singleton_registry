from singleton import MetaSingleton
from device import IoTDevice
from registry import Registry


# Функция для создания нового устройства
def create_device():
    serial_number = input("Введите серийный номер устройства: ")
    name = input("Введите имя устройства: ")
    return IoTDevice.create_device(serial_number, name)


# Функция для регистрации устройства в реестре
def register_device(registry, device):
    registry.register_device(device.serial_number, device)
    print(f"Устройство {device.name} зарегистрировано.")


# Функция для поиска устройства по серийному номеру
def search_device(registry):
    search_serial_number = input("Введите серийный номер для поиска: ")
    retrieved_device = registry.find_device_by_serial_number(search_serial_number)

    if retrieved_device:
        print(f"Найдено устройство(-ва) с серийным номером {search_serial_number}:")
        for device in retrieved_device:
            print(str(device))
    else:
        print(f"Устройство с серийным номером {search_serial_number} не найдено")


# Функция для вывода всех зарегистрированных устройств
def print_registered_devices(registry):
    print("Зарегистрированные устройства:")
    for serial, device_list in registry._registry.items():
        for device in device_list:
            print(str(device))


# Основная функция программы
def main():
    registry = Registry()
    device = None

    while True:
        print("\n1. Создать устройство")
        print("2. Зарегистрировать устройство")
        print("3. Поиск устройства по серийному номеру")
        print("4. Вывести зарегистрированные устройства")
        print("5. Выход")

        choice = input("Выберите действие (1/2/3/4/5): ")

        if choice == "1":
            device = create_device()
        elif choice == "2":
            if device is not None:
                register_device(registry, device)
            else:
                print("Сначала создайте устройство.")
        elif choice == "3":
            search_device(registry)
        elif choice == "4":
            print_registered_devices(registry)
        elif choice == "5":
            break
        else:
            print("Некорректный выбор. Пожалуйста, выберите 1, 2, 3, 4 или 5.")


if __name__ == '__main__':
    main()
