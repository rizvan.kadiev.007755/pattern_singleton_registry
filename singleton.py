"""
Описание работы класса MetaSingleton:

1. Статическое поле `_instances`: Внутри метакласса определено статическое поле `_instances`,
которое служит для хранения созданных экземпляров классов.

2. Метод `__call__`: Метод `__call__` является основным механизмом, который позволяет
метаклассу контролировать создание экземпляров классов.
Этот метод вызывается при попытке создать новый экземпляр класса, и в нем выполняется следующая логика:
    - Проверяется, существует ли уже экземпляр класса в словаре `_instances`.
    - Если экземпляр класса не существует, то вызывается конструктор класса `super().__call__(*args, **kwargs)` для создания нового экземпляра.
    - Новый экземпляр добавляется в словарь `_instances` с ключом, равным классу (`cls`).
    - Метод возвращает экземпляр класса.

Класс `MetaSingleton` позволяет обеспечить, что для каждого класса,
использующего этот метакласс, будет существовать только один экземпляр,
независимо от того, сколько раз он попытается быть созданным.
Это обеспечивает гарантированный единственный экземпляр (Singleton) для таких классов.

"""


class MetaSingleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super().__call__(*args, **kwargs)
        return cls._instances[cls]
